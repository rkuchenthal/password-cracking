#include <stdio.h>
#include <string.h>
#include "md5.h"
#include <stdlib.h>

int main(int argc, char*argv[])
{
    if( argc < 2 || argc > 3)
    {
        printf("Usage: %s word_to_hash [output_file]\n",argv[0]);
        exit(1);
    }
     
     FILE*in = fopen(argv[1],"r");
     FILE*out = fopen(argv[2],"w");
    if(argc == 3)
    {
            
            if (!out)
            {
                printf("Can't open %s for writing.\n", argv[2]);
                exit(2);
            }
            
    }  
    else
     {
       out = stdout;
     }       
            
    char line[15];
     while(fgets(line, 15, in) )
      {
           
        for(int i = 0; i < strlen(line); i++)
          {
            if(line[i] == '\n')
             {
                 line[i] ='\0';
             }
             
          }
          char *hash = md5(line, strlen(line));
          fprintf(out, "%s\n", hash);
          
      }
     fclose(out);
     fclose(in);
           
}